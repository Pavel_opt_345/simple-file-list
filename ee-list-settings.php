<?php // ee-list-settings.php
	
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
if ( ! wp_verify_nonce( $nonce, 'ee_list_settings' ) ) exit; // Exit if nonce fails
	
// Check for POST and Nonce
if(@$_POST['eePost'] AND check_admin_referer( 'ee-simple-file-list-settings', 'ee-simple-file-list-settings-nonce')) {
		
	// Store our settings in the options table as a pipped string.
	// We'll turn this into an array on the other side.
	
	// Data validation and sanitizing much improved - 11.17.15
	
	// Only accept Yes as the answer string.
	$eeSettings = 'eeAllowList=';
	if($_POST['eeAllowList'] == 'Yes') { $eeSettings .= 'Yes'; } else { $eeSettings .= 'No'; }
	$eeSettings .= '|';
	
	// Only accept Yes as the answer string. Gotta stay positive!
	$eeSettings .= 'eeAllowUploads=';
	if($_POST['eeAllowUploads'] == 'Yes') { $eeSettings .= 'Yes'; } else { $eeSettings .= 'No'; }
	$eeSettings .= '|';
	
	// This must be a number
	$ee_upload_max_filesize = (int) $_POST['ee_upload_max_filesize'];
	// Can't be more than the system allows.
	if(!$ee_upload_max_filesize OR $ee_upload_max_filesize > $ee_post_max_size) { $ee_upload_max_filesize = $ee_post_max_size; }
	$eeSettings .= 'ee_upload_max_filesize=' . $ee_upload_max_filesize . '|';
	
	// Strip all but what we need for the comma list of file extensions
	$eeSettings .= 'eeFormats=' . preg_replace("/[^a-z0-9,]/i", "", $_POST['eeFormats']) . '|';
	
	// Need a good email. It's okay if there isn't one. No messages will be sent.
	// Relying on input[type=email] to catch user input errors.
	if($_POST['eeAdminTo'] AND filter_var($_POST['eeAdminTo'], FILTER_SANITIZE_EMAIL)) {
		$eeSettings .= 'eeAdminTo=' . $_POST['eeAdminTo'];
	} else {
		$eeSettings .= "eeAdminTo="; // Anything but a good email gets null.
	}
	
	// Only accept Yes as the answer string.
	$eeSettings .= '|eeFileOwner=';
	if($_POST['eeFileOwner'] == 'Yes') { $eeSettings .= 'Yes'; } else { $eeSettings .= 'No'; }
	
	
	// Build the query
	$eeQuery = "UPDATE " . $wpdb->options . " SET option_value = '" . $eeSettings . "' WHERE option_name = 'eeSFL'";
	
	// Run the query
	if($wpdb->query($eeQuery)) {
		$eeConfirm = 'Simple File List Settings Have Been Updated!';
	} else {
		$eeLog[] = @mysqli_error($wpdb);
	}
}


// Get current info...
$eeQuery = "SELECT option_value FROM " . $wpdb->options . " WHERE option_name = 'eeSFL'";

// Run the query
$eeResult = $wpdb->get_results($eeQuery, ARRAY_N);

if($eeResult) {
	
	// Get the result
	$eeSettings = $eeResult[0][0];
	$eeLog[] = $eeSettings;
	$eeSettings = explode('|', $eeSettings);
	
	// Set our variables
	$eeAllowList = explode('=', $eeSettings[0]);
	$eeAllowList = $eeAllowList[1];
	
	$eeAllowUploads = explode('=', $eeSettings[1]);
	$eeAllowUploads = $eeAllowUploads[1];
	
	$ee_upload_max_filesize = explode('=', $eeSettings[2]);
	$ee_upload_max_filesize = $ee_upload_max_filesize[1];
	
	$eeFormats = explode('=', $eeSettings[3]);
	$eeFormats = $eeFormats[1];
	
	$eeAdminTo = explode('=', $eeSettings[4]);
	$eeAdminTo = $eeAdminTo[1];
	
	$eeFileOwner = explode('=', $eeSettings[5]);
	$eeFileOwner = $eeFileOwner[1];

}
	
?>

<div class="eeAdminEntry">
	
	<?php
		
		if($eeConfirm) { echo '<div class="updated"><p>' . $eeConfirm . '</p></div>'; }
	
	?>
	
	<p>USAGE: Place this bit of shortcode in any Post or Page that you would like the plugin to appear: <strong><em>[eeSFL]</em></strong></p>
	
	<form action="<?php echo $_SERVER['PHP_SELF'] . '?page=ee-simple-file-list-admin'; ?>" method="post">
		<input type="hidden" name="eePost" value="TRUE" />	
		
		<?php wp_nonce_field( 'ee-simple-file-list-settings', 'ee-simple-file-list-settings-nonce' ); ?>
		
		<fieldset>
		
			<h2>The List</h2>
			<span>Show the File List?</span><label for="eeListYes" class="eeRadioLabel">Yes</label><input type="radio" name="eeAllowList" value="Yes" id="eeListYes" <?php if($eeAllowList == 'Yes') { echo 'checked'; } ?> />
				<label for="eeListNo" class="eeRadioLabel">No</label><input type="radio" name="eeAllowList" value="" id="eeListNo" <?php if($eeAllowList != 'Yes') { echo 'checked'; } ?> />
				<br class="eeClearFix" />
				<div class="eeNote">Affects the front-end uploader only. You can use the uploader without showing the file list.</div>
			<br class="eeClearFix" />
			
		</fieldset>
		<fieldset>
			
			<h2>The Uploader</h2>
			<span>Allow File Uploads?</span><label for="eeUploadYes" class="eeRadioLabel">Yes</label><input type="radio" name="eeAllowUploads" value="Yes" id="eeUploadYes" <?php if($eeAllowUploads == 'Yes') { echo 'checked'; } ?> />
				<label for="eeUploadNo" class="eeRadioLabel">No</label><input type="radio" name="eeAllowUploads" value="" id="eeUploadNo" <?php if($eeAllowUploads != 'Yes') { echo 'checked'; } ?> />
					<div class="eeNote">Affects the front-end uploader only. Files will be uploaded to...<br />
						<strong><?php echo $eeUploadLink; ?></strong>
					</div>
			<br class="eeClearFix" />
			
			
			<label for="ee_upload_max_filesize">How Big? (MB):</label><input type="number" min="1" max="<?php echo $ee_post_max_size; ?>" step="1" name="ee_upload_max_filesize" value="<?php echo $ee_upload_max_filesize; ?>" class="eeAdminInput" id="ee_upload_max_filesize" />
				<div class="eeNote">Your hosting limits the maximum file upload size to <strong><?php echo $ee_post_max_size; ?> MB</strong>.</div>
			
			<br class="eeClearFix" />
			<label for="eeFormats">Allowed Types:</label><textarea name="eeFormats" class="eeAdminInput" id="eeFormats" /><?php echo $eeFormats; ?></textarea>
				<div class="eeNote">Only use the file types you absolutely need, ie; jpg, jpeg, png, pdf, mp4, etc</div>
				
				
			<span>Add Owner?</span><label for="eeFileOwnerYes" class="eeRadioLabel">Yes</label><input type="radio" name="eeFileOwner" value="Yes" id="eeFileOwnerYes" <?php if($eeFileOwner == 'Yes') { echo 'checked'; } ?> />
				<label for="eeFileOwnerNo" class="eeRadioLabel">No</label><input type="radio" name="eeFileOwner" value="" id="eeFileOwner" <?php if($eeFileOwner != 'Yes') { echo 'checked'; } ?> />
					<div class="eeNote">Appends the logged-in Wordpress username to the file name.</div>
			<br class="eeClearFix" />	
				
				
				
			<label for="eeAdminTo">Notice Email:</label><input type="email" name="eeAdminTo" value="<?php echo $eeAdminTo; ?>" class="eeAdminInput" id="eeAdminTo" size="64" />
				<div class="eeNote">You'll get an email whenever a file is uploaded.</div>
			
		</fieldset>
		
		<fieldset>
		
			<input type="submit" name="submit" id="submit" value="SAVE" />
			
			<p><?php echo $eeDisclaimer; ?><br />
			<a href="mailto:info@mysite.com">Bug Reports / Feedback</a></p>
			
			<p><a class="eeBacklink" href="<?php echo $eeBackLink; ?>" target="_blank"><?php echo $eeBackLinkTitle; ?></a></p>
		
		</fieldset>
		
	</form>
</div>
