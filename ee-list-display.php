<?php // Fitting Room - ee-list-display.php
	
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
if ( ! wp_verify_nonce( $nonce1, 'ee_main_page_display' ) ) exit; // Exit if nonce fails



//echo "ee-list-display".$eeSelectedCategory;
//echo  "g_lobal".$G_LOBALS['eeSelectedCategory'];

if(@$_POST['eeUpload'])
{
//echo "it is upload";
	if (!empty($_POST['ee_subFolder']))
	{
   $_SESSION['eeSelectedCategory']=$_POST['ee_subFolder'];
    $eeSelectedCategory=$_POST['ee_subFolder'];
  }
}
else
$eeSelectedCategory=$_SESSION['eeSelectedCategory'];	
// If Delete Files...

if(@$_POST['eeListForm']) {
//echo "it is delete";
	foreach($_POST['eeDeleteFile'] as $eeKey => $eeFile){
		
		if(unlink($eeFile)) {
			$eeMsg = 'Deleted the file &rarr; ' . $eeFile;
			$eeLog[] = $eeMsg;
			$eeMessages[] = $eeMsg;
		} else {
			$eeMsg = 'Could not delete the file: ' . $eeFile;
			$eeLog[] = $eeMsg;
			$eeMessages[] = $eeMsg;
		}
	}
	
}

if($eeMessages) { 
	echo '<div id="eeMessaging" class="updated">';
	eeMessageDisplay($eeMessages);
	echo '</div>';
} ?>

<?php if($eeErrors) { 
	echo '<div id="eeMessaging" class="error">';
	eeMessageDisplay($eeMessages);
	echo '</div>';
}

// List files in folder, add to array. //add subfolder


if ($eeHandle = @opendir($eeUploadDir."/".$eeSelectedCategory."/")) {

	while (false !== ($eeFile = readdir($eeHandle))) {
		if (!@in_array($eeFile, $eeExcluded)) {
			if (!@is_dir($eeFile)) {
				$eeFiles[] = $eeFile; // Add the file to the array
			}	
		}
	}
	@closedir($eeHandle);
	
	
	// Sort Array Alphabetically
	@natcasesort($eeFiles);
	
	$eeFileCount = count($eeFiles);
	
	if($eeFileCount) {
		
		$eeMsg = "There are " . $eeFileCount . " files in the list";
		
		$eeLog[] = $eeMsg;
		
		if($eeAdmin) {
		
		?>
		
	<form action="<?php echo $_SERVER['PHP_SELF'] . '?page=ee-simple-file-list'; ?>" method="post">	
		<input type="submit" name="eeListForm" value="Delete Checked" class="alignright" />
		
	<?php } ?>
	
	<p><?php echo $eeMsg; ?></p>
	
	<table class="eeFiles tablesorter">
		<thead>
			<tr>
				<th>File</th>
				<th class="sortless">Size</th>
				<th>Date</th>
				<?php if($eeAdmin) { echo "<th>Delete</th>"; } ?>
			</tr>
		</thead>
		
		<tbody>
		
		<?php
	// $path - ���� � ����������
// $pattern - ������ ������
// $flags - ��������� ��� ������� glob()
// $depth - ������� �����������, ��������������� ��������. -1 - ��� �����������.
function bfglob($path, $pattern = '*', $flags = GLOB_NOSORT, $depth = 0)
  {
  $matches = array();
  $folders = array(rtrim($path, DIRECTORY_SEPARATOR));

  while($folder = array_shift($folders))
    {
    $matches = array_merge($matches, glob($folder.DIRECTORY_SEPARATOR.$pattern, $flags));
    if($depth != 0)
      {
      $moreFolders = glob($folder.DIRECTORY_SEPARATOR.'*', GLOB_ONLYDIR);
      $depth   = ($depth < -1) ? -1: $depth + count($moreFolders) - 2;
      $folders = array_merge($folders, $moreFolders);
      }
    }
  return $matches;
  }

		//save to file
	 $file = bfglob($eeUploadDir, "*", GLOB_NOSORT, -1);
//    $serializedData = json_encode($file); //where '$array' is your array
  //  file_put_contents($eeUploadDir.'files.txt', $serializedData);

			$fp = @fopen($eeUploadDir."/files.txt", "w+");
		foreach($file as $entry)
 {
//&& is_dir($eeUploadDir."/".$entry) 
  if ("." != $entry && ".." != $entry ) 
  {
    @fwrite($fp, $entry.";".date('c' ,@filemtime($entry)).PHP_EOL );
    // 
  }
  }
		@fclose($fp);
		// Loop through array
		foreach($eeFiles as $eeKey => $eeFile) {
			
			if(strpos($eeFile, '.') !== 0) { // Don't display hidden files
			//save to file 
   //     @fwrite($fp, $eeFile.";".date('c' ,@filemtime($eeUploadDir . $eeFile)).PHP_EOL );
			// end save
				echo '<tr>';
				
				// File Name
				//echo '<td><a href="' . $eeUploadLink . $eeFile .  '" target="_blank">'. $eeFile . '</a></td>';
				echo '<td>'. $eeFile . '</td>';
				
				// File Size
				echo '<td>' . eeBytesToSize(filesize($eeUploadDir .$eeSelectedCategory."/".$eeFile)) . '</td>';
				
				// File Modification Date
				echo '<td>' .  date('c' ,@filemtime($eeUploadDir .$eeSelectedCategory."/". $eeFile)). '</td>';
				
				if($eeAdmin) {
					echo '<td><input type="checkbox" name="eeDeleteFile[]" value="'.$eeUploadDir .$eeSelectedCategory."/". $eeFile . '" /></td>';
				}
				
				echo '</tr>';
			
			}		
			
		} // END loop
		
  //  @fclose($fp);
	
		echo '</tbody></table>';
	 
	 }
	
	if($eeAdmin) { echo "</form>"; }

} else {

	$eeLog[] = 'Error: Can\'t read the files in the Uploads folder. ';
}

if (count($eeFiles) < 1) {
	echo "<p>There are no files to list.</p>";
}

?>