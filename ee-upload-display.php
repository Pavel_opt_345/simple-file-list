<?php  // File UPloader - ee-upload-display.php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
if ( ! wp_verify_nonce( $nonce2, 'ee_upload_display' ) ) exit; // Exit if nonce fails

// User Messaging	
if($eeMessages) { 
	echo '<div id="eeMessaging" class="updated">';
	eeMessageDisplay($eeMessages);
	echo '</div>';
}
	
if($eeErrors) { 
	echo '<div id="eeMessaging" class="error">';
	eeMessageDisplay($eeErrors);
	echo '</div>';
}

// Clear before the list is loaded
$eeErrors = array();
$eeMessages = array();

	
if ($ee_upload_max_filesize) {
	
	// Decide where to submit the form
	if($eeAdmin) {
		$eeAction = $_SERVER['PHP_SELF'] . '?page=ee-simple-file-list';
	} else {
		$eeAction = get_permalink();
	}
	
	?>

	<form action="<?php echo $eeAction; ?>" method="post" enctype="multipart/form-data">
		<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo (($ee_upload_max_filesize*1024)*1024); // Convert to bytes. ?>" />
		<input type="hidden" name="eeUpload" value="TRUE" />
		
		<?php // This is checked in ee_main_page_display() 
		//$eeSelectedCategory=$_COOKIE["eeSelectedCategory"];

		$eeSelectedCategory=$_SESSION['eeSelectedCategory'];
				
		//echo "--".$eeSelectedCategory."--";

			echo '<p><select  name="ee_subFolder" value='.$eeSelectedCategory.'>';

	$filelist = array();
	if ($handle = opendir($eeUploadDir)) {
	    while ($entry = readdir($handle)) {
            $filelist[] = $entry;
if ("." != $entry && ".." != $entry  && is_dir($eeUploadDir."/".$entry)) 
{
  $selected="";
  if ($entry==$eeSelectedCategory)
  {
  $selected=" selected ";
  }
echo '<option ',$selected,'>', htmlspecialchars($entry),'</option>';
}
	    }

	    closedir($handle);
	//echo  $entry.PHP_EOL;
	
	}
echo '</select></p>';

			wp_nonce_field( 'ee-simple-file-list-upload', 'ee-simple-file-list-upload-nonce' ); ?>
	
		
		<p class="alignright"><?php echo $ee_upload_max_filesize; ?> MB Upload Size Limit</p>
		<input type="submit" value="See files in Category" class="eeUploadButton" />
		<BR> 
	    <h1>Upload a File</h1>
	    
		<input type="file" name="eefile" />
		<input type="submit" value="Upload the File" class="eeUploadButton" />
		
		<br class="eeClearFix" />
	
	</form>
	 <h1>Create new Category</h1>
	 <form action="<?php echo $_SERVER['PHP_SELF'] . '?page=ee-simple-file-list'; ?>" method="post">	
	  <input type="text" name="eeNewDir" value=""  >  
	  <input type="hidden" name="eeCreateDirUpload" value="TRUE" />
		<input type="submit" name="eeCreateDir" value="Create the Category" class="eeUploadButton">
		<BR>
		<BR>
		</form>		
<?php } ?>